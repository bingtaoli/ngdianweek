var dianControllers = angular.module('dianControllers', []);

dianControllers.controller("SiderCtrl", ['$scope', '$location', function($scope, $location){
	$scope.isActive = function(current){
		var href = '#'+$location.url();
	    return current === href;
	}
}]);

dianControllers.controller('LoginCtrl', function($scope, AuthService){
	$scope.login = function(){
		var credentials = {
			username: $scope.username,
			password: $scope.password
		};
		AuthService.login(credentials);
	}
});

dianControllers.controller('FillCtrl', function($scope, $http, AuthService){
	$scope.project_name = 'test';
	$scope.identify = 1;
	$scope.project_intro = 'test';
	if (AuthService.isAuthenticated() && AuthService.getProject()){
		$scope.project = AuthService.getProject();
		$scope.project_name = $scope.project.project_name;
		$scope.project_intro = $scope.project.project_intro;
		$scope.user = AuthService.getAuth();
		log($scope.user);
		$scope.username = $scope.user.username;
	} else {
		log('not auth or no project, go to login');
	}
	$scope.submitReport = function(){
		var data = {
			general_condition: $scope.generalCondition,
			delay_condition: $scope.delayCondition,
			week_meeting: $scope.weekMeeting
		};
		//本周活动
		var activity_description = [];
		if ($scope.adActivity){
			activity_description.push({
				activity: $scope.adActivity,
				state: $scope.adState,
				dutyMan: $scope.adDutyMan,
				remark: $scope.adRemark
			});
		}
		for (var i=0; i<$scope.moreActivities.length; i++){
			if ($scope.moreActivities[i].activity){
				activity_description.push($scope.moreActivities[i]);
			}
		}
		//下周活动
		var next_week_plan = [];
		if (next_week_plan){
			next_week_plan.push({
				activity: $scope.npActivity,
				state: $scope.npState,
				dutyMan: $scope.npDutyMan,
				remark: $scope.npRemark
			});
		}
		for (var i=0; i<$scope.moreNextActivities.length; i++){
			if ($scope.moreNextActivities[i].activity){
				next_week_plan.push($scope.moreNextActivities[i]);
			}
		}
		//问题跟踪
		var problem_track = [];
		if ($scope.ptDescription){
				problem_track.push({
				description: $scope.ptDescription,
				solution: $scope.ptSolution
			});
		}
		for (var i=0; i<$scope.moreProblems.length; i++){
			if ($scope.moreProblems[i].description){
				problem_track.push($scope.moreProblems[i]);
			}
		}
		//构造data
		data.activity_description = activity_description;
		data.next_week_plan = next_week_plan;
		data.problem_track = problem_track;
		//发送data
		log(data);
		var req = {
			method: 'POST',
			url: 'http://localhost/sae-dianweek/index.php/report/add_report',
			data: data
		};
		$http(req).success(function(){
			log("success");
		});
	};
	//活动
	$scope.moreActivities = [];
	$scope.addActivity = function(){
		//todo 当前活动必须先填写
		$scope.moreActivities.push({});
	}
	$scope.delActivity = function(index){
		$scope.moreActivities.splice(index, 1);
	}
	//下周活动
	$scope.moreNextActivities = [];
	$scope.addNextActivity = function(){
		$scope.moreNextActivities.push({});
	}
	$scope.delNextActivity = function(index){
		$scope.moreNextActivities.splice(index, 1);
	}
	//问题
	$scope.moreProblems = [];
	$scope.addProblem = function(){
		$scope.moreProblems.push({});
	}
	$scope.delProblem = function(index){
		$scope.moreProblems.splice(index, 1);
	}
	$scope.mates = [
		{
			name: 'MateA',
			contribute: 0,
			remark: 'good man',
		},
		{
			name: 'MateB',
			contribute: 0,
			remark: 'good man',
		},
		{
			name: 'MateC',
			contribute: 0,
			remark: 'good man',
		},
		{
			name: 'MateD',
			contribute: 0,
			remark: 'good man',
		},
	];
});

dianControllers.controller('ManageMateCtrl', function($scope){

});

dianControllers.controller('HistoryCtrl', function($scope){

});