'use strict';

var auth = angular.module('auth', []);

// auth内部使用，不对外提供API
auth.factory('Session', function ($window) {
    log('storage length is ', $window.sessionStorage.length);
    //init service data
    var user = angular.fromJson($window.sessionStorage.getItem('user'));
    if (user){
        log('user logged in ', user.username);
        this.username = user.username;
        this.identify = user.identify;
        this.project = user.project;
    }
    this.storeUser = function(json){
        $window.sessionStorage.setItem('user', JSON.stringify(json));
    }
    this.getUser = function(){
        return $window.sessionStorage.getItem('user');
    }
    return this;
});

//对外提供API
auth.factory('AuthService', function ($http, Session) {
    var authService = {};
    authService.login = function (credentials) {
        return $http
            .post('http://localhost/sae-dianweek/index.php/user/login', credentials)
            .then(function (res) {
                // str or int
                if (res.data.status == 1){
                    if (res.data.identify == 1){
                        log('login as master');
                        var json = {
                            username: credentials.username,
                            identify: 1,
                            project: res.data.project
                        };
                        Session.storeUser(json);
                    } else if (res.data.identify == 2){
                        log('login as root');
                        var json = {
                            username: credentials.username,
                            identify: 2,
                        };
                        Session.storeUser(json);
                    }
                } else {
                    log('login fail', res.data.message);
                }
            });
    };

    authService.isAuthenticated = function () {
        return !!Session.username;
    };

    authService.getProject = function(){
        if (Session.project){
            return Session.project;
        }
        return null;
    }

    authService.getAuth = function(){
        var data = {
            username: Session.username,
            identify: Session.identify,
        };
        return data;
    }

    return authService;
})